﻿function initNPC() {
    initXY();
    initBOSS();
}

function initXY() {
    g_yxArr = [
                   {
                       name: '罗伊德', life: 1300, STR: 175, DEF: 90, face: "yx1.jpg", skill: [
                           (function (npc) {
                               if (checkIsInEven(1, 5)) {
                                   this.name = '猛虎冲锋';
                                   npc.turnSTR_skill = 1;
                                   npc.tempSTR_skill = +100;
                                   return '<span class="jn">' + this.name + '</span>，伤害增加<span class="e-up">+100</span>';
                               }
                               else {
                                   return false;
                               }
                           }),
                           (function (npc) {
                               if (checkIsInEven(1, 5)) {
                                   this.name = '星辰暴击';
                                   npc.turnSTR_skill = 1;
                                   npc.tempSTR_skill = +180;
                                   npc.life -= 20;
                                   return '<span class="jn">' + this.name + '</span>，生命值<span class="e-down">-20</span>，伤害增加<span class="e-up">' + npc.tempSTR_skill + '</span>';
                               }
                               else {
                                   return false;
                               }
                           }),
                           (function (npc) {
                               if (checkIsInEven(3, 20)) {
                                   this.name = '勇猛之心';
                                   npc.life += 200;
                                   return '<span class="jn">' + this.name + '</span>，生命值<span class="e-up">' + npc.life + '</span>';
                               }
                               else {
                                   return false;
                               }
                           }),
                           (function (npc) {
                               if (checkIsInEven(1, 20)) {
                                   this.name = '陨星粉碎';
                                   npc.turnSTR_skill = 1;
                                   npc.tempSTR_skill = +580;
                                   return '<span class="max-jn">' + this.name + '</span>，伤害增加<span class="e-up">' + npc.tempSTR_skill + '</span>';
                               }
                               else {
                                   return false;
                               }
                           })
                       ]
                   },
                   { name: '银', life: 1400, STR: 125, DEF: 60, face: "yx2.jpg" },
                   {
                       name: '瓦吉', life: 1200, STR: 155, DEF: 50, face: "yx3.jpg", skill: [
                             (function (npc) {
                                 if (checkIsInEven(1, 5)) {
                                     this.name = '袭空天堂';
                                     npc.turnSTR_skill = 1;
                                     npc.tempSTR_skill = +100;
                                     return '<span class="jn">' + this.name + '</span>，伤害增加<span class="e-up">+100</span>';
                                 }
                                 else {
                                     return false;
                                 }
                             }),
                             (function (npc) {
                                 if (checkIsInEven(1, 5)) {
                                     this.name = '虚空之星';
                                     npc.turnSTR_skill = 1;
                                     npc.tempSTR_skill = +100;
                                     return '<span class="jn">' + this.name + '</span>，伤害增加<span class="e-up">+100</span>';
                                 }
                                 else {
                                     return false;
                                 }
                             }),
                             (function (npc) {
                                 if (checkIsInEven(1, 5)) {
                                     this.name = '苍蓝冲破';
                                     npc.turnSTR_skill = 1;
                                     npc.tempSTR_skill = +100;
                                     return '<span class="jn">' + this.name + '</span>，伤害增加<span class="e-up">+100</span>';
                                 }
                                 else {
                                     return false;
                                 }
                             }),
                             (function (npc) {
                                 if (checkIsInEven(1, 10)) {
                                     this.name = '虚空手臂';
                                     npc.turnSTR_skill = 1;
                                     npc.tempSTR_skill = +100;
                                     return '<span class="jn">' + this.name + '</span>，伤害增加<span class="e-up">+100</span>';
                                 }
                                 else {
                                     return false;
                                 }
                             })
                       ]
                   },
                   { name: '缇欧', life: 1100, STR: 105, DEF: 70, face: "yx4.jpg" }
    ];
}

function initBOSS() {
    g_bossArr = [
                { name: '食人花', life: 2500, STR: 155, DEF: 100, face: "boss1.jpg" },
                { name: '灵兽', life: 4000, STR: 225, DEF: 160, face: "boss2.jpg" },
                { name: '神机', life: 6000, STR: 325, DEF: 210, face: "boss3.jpg" },
                {
                    name: '瓦鲁多', life: 8000, STR: 425, DEF: 260, face: "boss4.jpg", type: 0, typeEven: [
                             (function (npc) {
                                 if (npc.type == 0 && npc.life <= (g_boss.life / 2)) {
                                     this.name = '魔人瓦鲁多';
                                     npc.life = npc.life * 1.5;
                                     npc.face = 'boss4-2.jpg';
                                     npc.type = 2;
                                     npc.tempSTR_skill = +100;
                                     npc.tempDEF_skill = +50;
                                     return '变身<span class="jn">' + this.name + '</span>，<span class="e-up">基础属性</span>大幅提升';
                                 }
                                 else {
                                     return false;
                                 }
                             })
                    ]
                },
                { name: '噬身之蛇-肯帕雷拉', life: 8000, STR: 525, DEF: 290, face: "boss5.jpg" },
                { name: '谢莉·奥兰多', life: 8000, STR: 625, DEF: 300, face: "boss6.jpg" },
                { name: '赤色战鬼 西格蒙德', life: 13000, STR: 725, DEF: 410, face: "boss7.jpg" },
                { name: '风之剑圣-亚里欧斯', life: 15000, STR: 825, DEF: 450, face: "boss8.jpg" },
                {
                    name: '噬身之蛇-阿瑞安赫德', life: 19000, STR: 925, DEF: 570, face: "boss9.jpg", type: 0, typeEven: [
                        (function (npc) {
                            if (npc.type == 1 && npc.life <= (g_boss.life / 3)) {
                                npc.face = 'boss9-2.jpg';
                                npc.type = 2;
                                npc.tempSTR_skill = +400;
                                npc.tempDEF_skill = +150;
                                return '<span class="jn"></span>，<span class="e-up">基础属性</span>大幅提升';
                            }
                            else {
                                return false;
                            }
                        }),
                        (function (npc) {
                            if (npc.type == 0 && npc.life > (g_boss.life / 3) && npc.life <= (g_boss.life * 2 / 3)) {
                                npc.face = 'boss9-1.jpg';
                                npc.type = 1;
                                npc.tempSTR_skill = +400;
                                npc.tempDEF_skill = +150;
                                return '变身<span class="jn"></span>，<span class="e-up">基础属性</span>大幅提升';
                            }
                            else {
                                return false;
                            }
                        })
                    ],
                    skill: [
                        (function (npc) {
                            if (checkIsInEven(1, 5)) {
                                this.name = '大地轰雷锤';
                                npc.turnSTR_skill = 1;
                                npc.tempSTR_skill = +200;
                                return '<span class="jn">' + this.name + '</span>，伤害增加<span class="e-up">' + npc.tempSTR_skill + '</span>';
                            }
                            else {
                                return false;
                            }
                        }),
                        (function (npc) {
                            if (checkIsInEven(1, 10)) {
                                this.name = '圣技·大十字';
                                npc.turnSTR_skill = 1;
                                npc.tempSTR_skill = +500;
                                return '变身<span class="jn">' + this.name + '</span>，<span class="e-up">基础属性</span>大幅提升';
                            }
                            else {
                                return false;
                            }
                        })
                    ]
                },
                { name: 'God-老师大人', life: 102400, STR: 1024, DEF: 1024, face: "boss10.jpg" }
    ];
}

function getBaseBoss() {
    return m_boss = {
        name: '野怪',
        life: 1300,
        STR: 100,
        DEF: 50,
        face: 'baseBoss.jpg'
    };
}

function runFace(npc) { // 是否血量降到一定值后，更换头像
    var tx = '';
    if (npc.typeEven) {
        $.each(npc.typeEven, function (i, v) {
            tx = v(npc);
            if (tx) {
                tx += '<br/>';
                return false;
            }
            else {
                tx = '';
            }
        });
    }
    return tx;
}

function runSkill(npc) {
    if (npc.skill) {
        var m_evenIndex = Math.floor(Math.random() * npc.skill.length);
        if (npc.skill[m_evenIndex] != undefined) {
            var str = npc.skill[m_evenIndex](npc);
            if (str) {
                return '发动技能：' + str + '<br/>';
            }
            else {
                return "";
            }
        }
        else {
            return "";
        }
    }
    else {
        return "";
    }
}