﻿function initYXEven() {
    var ev1 = function (npc) {
        if (checkIsInEven(7, 10)) {
            npc.turnSTR = 2;
            npc.tempSTR = -npc.STR * 0.5;
            return '秀发被风吹乱，2回合内攻击力下降<span class="e-down">-50%</span>，';
        }
        else {
            return false;
        }
    }

    var ev2 = function (npc) {
        npc.turnSTR = 1;
        npc.tempSTR = -100;
        return '攻击被对方格挡，攻击伤害降低<span class="e-down">-100</span>';
    }

    var ev3 = function (npc) {
        if (checkIsInEven(1, 100)) {
            npc.life = 1;
            npc.turnSTR = 1;
            npc.tempSTR = npc.STR * 0.5;
            return '膝盖中了一箭，生命值降为<span class="e-set">1</span>，发起最后一击';
        }
        else {
            return false;
        }
    }

    var ev4 = function (npc) {
        if (checkIsInEven(3, 10)) {
            npc.turnDEF = 1;
            npc.tempDEF = +50;
            npc.turnSTR = 1;
            npc.tempSTR = -npc.STR;
            return '摆出格挡姿势，1回合内防御力<span class="e-up">+50</span>';
        }
        else {
            return false;
        }
    }
    g_YXEvenArr = [ev1, ev2, ev3, ev4];
}

function runYXEven(npc) {
    if (g_YXEvenArr.length > 0) {
        var m_evenIndex = Math.floor(Math.random() * (g_YXEvenArr.length + 10));
        if (g_YXEvenArr[m_evenIndex] != undefined) {
            var str = g_YXEvenArr[m_evenIndex](npc);
            if (str) {
                return str + '<br/>';
            }
            else {
                return "";
            }
        }
        else {
            return "";
        }
    }
    else {
        return "";
    }
}