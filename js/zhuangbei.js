﻿function initZhuangBeiEven() {

    var ev1 = function (npc) {
        if (checkIsInEven(5, 30)) {
            var m_buff = npc.STR * 0.1;
            npc.STR += m_buff;

            if (npc.face.indexOf('yx1') > -1) {
                npc.face = 'yx1-1.jpg';
            }
            g_yx = npc;
            return '在路边发现<span class="zb">打狗棒</span>，攻击力永久<span class="e-up">' + m_buff + '</span>点,';
        }
        else {
            return false;
        }
    }

    var ev2 = function (npc) {
        var m_buff = npc.DEF * 0.1;
        npc.DEF += m_buff;
        if (npc.face.indexOf('yx1') > -1) {
            npc.face = 'yx1-2.jpg';
        }
        g_yx = npc;
        return '穿上<span class="zb">校服上衣</span>，防御力永久<span class="e-up">' + m_buff + '</span>点,';

    }

    var ev3 = function (npc) {
        var m_buff = npc.DEF * 0.2;
        npc.DEF += m_buff;
        g_yx = npc;
        return '穿上<span class="zb">我的滑板鞋</span>，防御力永久<span class="e-up">' + m_buff + '</span>点,';
    }

    g_ZhuangBeiArr = [ev1, ev2, ev3];
}

function initZhuangBeiList(p_index, p_even) {
    g_ZhuangBeiArr[p_index] = p_even;
}

function checkIsInEven(p_x, p_size) {
    var m_Index = Math.ceil(Math.random() * p_size);
    if (m_Index <= p_x) {
        return true;
    }
    else {
        return false;
    }
}

function runZhuangbeiEven(npc) {
    if (g_ZhuangBeiArr.length > 0) {
        var m_evenIndex = Math.floor(Math.random() * g_ZhuangBeiArr.length * 2); // 随机范围是装备列表的2倍长度
        if (g_ZhuangBeiArr[m_evenIndex] != undefined) {
            var str = g_ZhuangBeiArr[m_evenIndex](npc);
            if (str) {
                g_ZhuangBeiArr.splice(m_evenIndex, 1); // 清除事件，不允许再发生该事件
                return str + '<br/>';
            }
            else {
                return "";
            }
        }
        else {
            return "";
        }
    }
    else {
        return "";
    }
}